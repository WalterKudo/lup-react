import React from 'react';
import '../header/header.css'
import { Link } from 'react-router-dom';
import Logo from '../../Assets/logo.png'
import redesSociais from '../../Assets/sociais.png'
import { FiMail } from "react-icons/fi";
import { GiSmartphone } from "react-icons/gi";
import { IoMdArrowDropdown } from "react-icons/io";



export default function Header(){
function openMenu(e){
    const menu = document.querySelector('.menu-container-responsive')
    const body = document.querySelector('body')
    e.preventDefault();
    menu.classList.add('menu_open')
    body.classList.add('menu-body')
}
function closeMenu(e){
    const menu = document.querySelector('.menu-container-responsive')
    const body = document.querySelector('body')
    e.preventDefault();
    menu.classList.remove('menu_open')
    body.classList.remove('menu-body')
}

function dropdownOpen(e){
    const dropdown = document.querySelector('.dropdown-mobile')
    e.preventDefault();
    dropdown.classList.toggle("dropdown_open");   
}
function dropdownOpenLast(e){
    const dropdown = document.querySelector('.dropdown-mobile-last')
    e.preventDefault();
    dropdown.classList.toggle("dropdown_open");   
}

return(
  
    <header className="header">
        <div>
        {/* Header Desktop */}
        <div className="container header-desktop">
            <div>
            <Link to="/"> <img src={Logo} alt="logo"/></Link>
            </div>
            <div className="menu-container">
                <div className="contato">
                    <div>
                        <FiMail color="#a4416f" size="22" />
                        <span>contato@contato.com.br</span>
                    </div>
                    <div>
                        <GiSmartphone color="#a4416f" size="22"/>
                        <span>+55 (15) 3249-2296</span>
                    </div>
                    <div>
                        <img src={redesSociais} alt="redes sociais"/>
                    </div>
                </div>
                <div className="menu">
                    <nav>
                        <Link to="/">Home</Link> 
                        <a href="https://drops.lup.com.br/">Blog</a> 
                        <div  className="dropdown">
                            <h2>Consultar<span><IoMdArrowDropdown/></span></h2> 
                            <div className="dropdowncontent">
                                <Link className="linkDropdown" to="/">Convite</Link> 
                                <Link className="linkDropdown" to="/">Anunciantes</Link> 
                                
                                <Link className="linkDropdown dropdowneventos" to="/">Eventos Publicos</Link> 
                            </div>
                        </div>
                        <div  className="dropdown">
                            <h2>Cadastro<span><IoMdArrowDropdown/></span></h2> 
                            <div className="dropdowncontent">
                                <Link className="linkDropdown" to="/">Anunciante</Link> 
                                <Link className="linkDropdown" to="/">Consumidor</Link> 
                            </div>
                        </div>
                        <a href="/">Quem somos</a> 
                        <a href="/">Contato</a> 
                    </nav>
                    <button>Login</button>
                </div>
            </div>
        </div>	
        {/* Header Mobile */}

        <div className="header-mobile">
            
            <div className="logo">
                <Link to="/"> <img src={Logo} alt="logo"/></Link>
            </div>
            <div className="header-esquerda">
            <nav className="menu">
                    
                    <span className="btnMenu btnMenuOpen" onClick={openMenu}>menu</span>  
                    
                    <div className="menu-container menu-container-responsive">
                    <div onClick={closeMenu} className="overlay"></div>
                    <div className="menu-header">    
                        <div className="logo">
                            <Link to="/"> <img src={Logo} alt="logo"/></Link>
                        </div>
                        <div className="navbar-close">
                            <span className="btnMenu btnMenuClose" onClick={closeMenu}>menu</span>
                        </div>
                    </div>  
                    <ul>
                        <li><Link to="/">Home</Link></li> 
                        
                        <li><a href="https://drops.lup.com.br/">Blog</a></li> 
                        
                        <li onClick={dropdownOpen}>
                            <span>Consultar<span><IoMdArrowDropdown/></span></span>       
                       
                            <div className="dropdown-mobile">
                                <div><Link to="/">Convite</Link></div> 
                                <div><Link to="/">Anunciantes</Link></div> 
                                <div><Link to="/">Eventos Publicos</Link></div> 
                            </div> 
                        </li>
                        <li onClick={dropdownOpenLast}>
                            <span>Cadastro<span><IoMdArrowDropdown/></span></span> 
                        
                            <div className="dropdown-mobile-last">
                                <div><Link to="/">Anunciante</Link></div> 
                                <div><Link to="/">Consumidor</Link></div> 
                            </div>
                        </li>
                        <li><a href="/">Quem somos</a></li> 
                        <li><a href="/">Contato</a></li> 
                    </ul>
                    <div className="footer-menu">
                    <div>
                        <FiMail color="#a4416f" size="14" />
                        <span>contato@contato.com.br</span>
                    </div>
                    <div >
                        <GiSmartphone color="#a4416f" size="14"/>
                        <span>+55 (15) 3249-2296</span>
                    </div>
                    </div>
                    </div>

            </nav>
                <div className="menu-contato-responsive">
                    <div>
                        <img src={redesSociais} alt="redes sociais"/>
                    </div>
                    <div>
                        <FiMail color="#a4416f" size="22" />
                        <span>contato@contato.com.br</span>
                    </div>
                    <div>
                        <GiSmartphone color="#a4416f" size="22"/>
                        <span>+55 (15) 3249-2296</span>
                    </div>
                   
                </div>
        </div>
        </div>

        </div>

    </header>

)}