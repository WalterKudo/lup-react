import React from 'react';
import './Footer.css';
import cartoes from './../../Assets/bandeiras.png'

export default function Footer(){
return(
    <footer>
        <div className="footer-container">
            <div className="footer-description">
                <h1>Lorem ipsum dolor sit amet</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquet turpis id efficitur vestibulum. Fusce bibendum sem vel molestie laoreet. Curabitur quis magna faucibus, venenatis odio vel</p>
            </div>
            <div className="footer-list">
                <ul>
                    <li>Lorem ipsum dolor</li>
                    <li>Lorem</li>
                    <li>Lorem</li>
                </ul>
                <ul>
                    <li>Lorem ipsum dolor</li>
                    <li>Lorem</li>
                    <li>Lorem</li>
                </ul>
                <ul>
                    <li>Lorem ipsum dolor</li>
                    <li>Lorem</li>
                    <li>Lorem</li>
                    <li>Lorem</li>
                </ul>
            </div>
        </div>
        <div className="cartoes" >
        <img src={cartoes} alt="bandeiras de catões"></img>
        </div>
    </footer>
)
}