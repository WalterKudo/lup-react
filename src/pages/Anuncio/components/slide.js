import React,{useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import api from '../../../Services/api';
import { IoIosArrowForward } from "react-icons/io";
import { IoIosArrowBack } from "react-icons/io";
import '../css/slide.css'

export default function Avaliacoes(){

    let params = useParams()

    const [imagens, setImagens] = useState([])

    useEffect(()=>{

        async function loadRate(){
            const response = await api.get(`/anuncio/galeria_fotos/${params.id}`)
            console.log(response)
                
            
            setImagens(response.data)
        } 
        loadRate()
    },[])

    const track = document.querySelector('.slide-track')

    console.log(track)

    return(
     
        <div className="slide">
            <button className="btn-slide-left"><IoIosArrowBack size="50"/></button>
            <div className="slide-container">
                <ul className="slide-track">
                {imagens.map(imagem => (
                    
                        <li className="slide-img" key={imagem.id}>
                            <img src={`http://18.221.183.124/lup_backend_master/public/storage/${imagem.caminho}`} />
                        </li>
                
                ))}
                </ul>
            </div>
            <button className="btn-slide-right" ><IoIosArrowForward size="50" /></button>
        
        </div>
        
        

    )



}
 
