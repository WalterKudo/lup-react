import React,{useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import api from '../../../Services/api';
import horarios from '../css/horarios.css'

export default function Avaliacoes(){

    let params = useParams()
    const [horarios, SetHorarios] = useState(['']);

    useEffect(()=>{

            async function loadHorarios(){

                const response = await api.get(`/anuncio/horario_atendimento/${params.id}`)

                console.log(response)

                for( var i=0; response.data.length > i; i++){
                    
                    let horaInicio = (response.data[i].horario_inicio)
                    horaInicio = horaInicio.slice(0,5)
                    response.data[i].horario_inicio = horaInicio

                    let horaFim = (response.data[i].horario_fim)
                    horaFim = horaFim.slice(0,5)
                    response.data[i].horario_fim = horaFim

                    console.log(horaFim)
                }
                
                SetHorarios(response.data)

            }
            loadHorarios();

    },[])
  
    return(
        
        <div>
            {horarios.map(horario => (
                           
            <table className="tabela-horarios" key={horario.id}>
                <thead>
                <tr>
                    <th>Dias</th>
                    <th>Horarios</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                
                    <td>{horario.dia_inicio} à {horario.dia_fim}</td>
                    <td>{horario.horario_inicio} até {horario.horario_fim}</td>    
                </tr>
                </tbody>

            </table>
            ))}
        </div>
        

    )



}