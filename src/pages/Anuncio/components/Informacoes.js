import React, {useState, useEffect} from 'react';
import { useParams } from "react-router-dom";
import api from '../../../Services/api';
import './../css/informacoes.css'


export default function Informacoes(){
    let params = useParams();

    const [data, setData] = useState([]);
    const [premio, setPremio] = useState([]);

    useEffect(()=>{

        async function loadExibir(){
            const response = await api.get(`/publico/anunciante/pagina/${params.id}`);
            
            setData(response.data)
        }

        loadExibir();
       
    },[])

    useEffect(()=>{
        async function loadPremio(){
            const response = await api.get(`/anuncio/premio/${params.id}`);

            setPremio(response.data)
            
        }
        loadPremio()

    },[])
    

    return(
        <div className="informacoes-body">
            <div>
                <h1>Serviços</h1>
                <p> {data.servico} </p>
            </div>
            <div>
                <h1>Contato/Telefone</h1>
                <p> {data.telefone} </p>
            </div>
        </div>
       
    )
    }