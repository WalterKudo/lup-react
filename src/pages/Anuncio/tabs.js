import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import './css/tabs.css';
import Informacoes from './components/Informacoes';
import Promocoes from './components/Promocoes';
import Avaliacoes from './components/Avaliacoes';
import Horarios from './components/Horarios';
import Slide from './components/slide'


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  
    

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div className="box-tables">{children}</div>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper, 
  }
 
  
}));

export default function ScrollableTabsButtonAuto() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  

  return (
    <div className={classes.root.tabs}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          <Tab label="Informações" {...a11yProps(0)} />
          <Tab label="Foto/Video" {...a11yProps(1)} />
          <Tab label="Promoções" {...a11yProps(2)} />
          <Tab label="Avaliações" {...a11yProps(3)} />
          <Tab label="Horários" {...a11yProps(4)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        
        <div><Informacoes/></div>
          
      </TabPanel>
      <TabPanel value={value} index={1}>
          <Slide/>
      </TabPanel>
      <TabPanel value={value} index={2}>
          <Promocoes/>
      </TabPanel>
      <TabPanel value={value} index={3}>
          <Avaliacoes/>
      </TabPanel>
      <TabPanel value={value} index={4}>
          <Horarios/>
      </TabPanel>
  
    </div>
  );
}