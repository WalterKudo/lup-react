import React, {useState, useEffect} from 'react';
import { useParams } from "react-router-dom";
import api from '../../Services/api';
import './css/Anuncio.css'
import image from '../../Services/img';
import Tab from './tabs';
import Footer from '../../components/Footer/Footer'

export default function Anuncio(){

    let params = useParams();

    const [data, setData] = useState([]);

    useEffect(()=>{

        async function loadExibir(){
            const response = await api.get(`/publico/anunciante/pagina/${params.id}`);
            
            setData(response.data)
        }

        loadExibir();
       
    },[])

    return(
        <>
        <div className="container-anuncio">
            <div className="btn-anuncio">
                <button>Solicitar orçamento</button>
            </div>
            <div className="main-anuncio">
                <div className="imagem-anuncio">
                    <img src={`${image}${data.foto_destaque}`}/>
                </div>
                <h1>{data.fantasia}</h1>
                <p>{data.descricao}</p>

            </div>
            <div>

            </div>
            <div className="container-tabs">
                <Tab/>
            </div>    

            
        </div>
        <Footer/>
        </>
    )
    }