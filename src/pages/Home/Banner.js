import React from 'react';
import './css/banner.css'
import { useHistory } from "react-router-dom";
export default function Banner(){
    let history = useHistory();
    console.log(history)

    function vai(){
        history.push('/anuncio')
    }

return(
    <article>
        <div className="banner">
        <div className="banner-container">
            <h1>
                Lorem ipsum dolor sit amet dolor sit amet
            </h1>
            <h2>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo felis eget vulputate facilisis. Integer mollis et eros in interdum
            </h2>
            <button onClick={vai}>
            Lorem ipsum dolor sit amet
            </button>
        </div>
        </div>

    </article>
)
}