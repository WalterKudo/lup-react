import React from 'react';
import './css/Galeria.css'
import imgbebe from './../../Assets/roupasbebe.png'
import imgcasal from './../../Assets/casal.jpg'



export default class Galeria extends React.Component{ 
 
    
render(){
 
    return(
    <>
    <section className="galeria">
        <div className="galeria-body">
            <div className="buffet">
                <h1>Buffet</h1>
            </div>
            <div className="casamento">
                <h1>Casamento</h1>
            </div>
            <div className="restaurante">
                <h1>Restaurante</h1>
            </div>
            <div className="foto">
                <h1>Foto e Vídeo</h1>
            </div>
            <div className="decoracao">
                <h1>Decoração</h1>
            </div>
            <div className="vestuario">
                <h1>Vestuário</h1>
            </div>
        </div>
    </section>
    <section className="galeria-posts">
        <div className="post-big">
            <h1>Lorem ipsum dolor</h1>
            <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>
            <div>
                <img src={imgbebe} alt="imagem"/>
            </div>
        </div>
        <div className="post-body">
            <div className="post">
                <img src={imgcasal} alt="imagem"/>
                <div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquet turpis id efficitur vestibulum. Fusce bibendum sem vel molestie laoreet. Curabitur quis magna faucibus, venenatis odio vel, vehicula augue.</p>
                </div>
            </div>
            <div className="post">
                <img src={imgcasal} alt="imagem"/>
                <div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquet turpis id efficitur vestibulum. Fusce bibendum sem vel molestie laoreet. Curabitur quis magna faucibus, venenatis odio vel, vehicula augue.</p>
                </div>
            </div>
            <div className="post">
                <img src={imgcasal} alt="imagem"/>
                <div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus aliquet turpis id efficitur vestibulum. Fusce bibendum sem vel molestie laoreet. Curabitur quis magna faucibus, venenatis odio vel, vehicula augue.</p>
                </div>
            </div>
        </div>
    </section>    
    </>
    )
}
}