import React from 'react';
import Banner from './Banner'
import Filtro from './Filtro'
import Galeria from './Galeria'
import Footer from '../../components/Footer/Footer'

export default function Home(history){

return(
    <>
    <Banner/>
    <Filtro />
    <Galeria/>
    <Footer/>
    </>
)
}