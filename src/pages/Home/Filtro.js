import React, {useEffect, useState} from 'react';
import './css/Filtro.css'
import casados from '../../Assets/casados.png'
import api from '../../Services/api'
import { useHistory } from "react-router-dom"

export default function Filtro(){
    let history = useHistory();

    const [estados, setEstados] = useState([]);
    const [estadoEscolhido, setEstadoEscolhido] = useState('');
    const [cidades, setCidades] = useState([]);
    const [escolhaCidade, setEscolhaCidade] = useState('')
    const [tipos, setTipos] = useState([]);
    const [escolhaTipos, setEscolhaTipos] = useState('');
    const [temas, setTemas] = useState([]);
    const [escolhaTema, setEscolhaTema] = useState([])


    useEffect(()=>{
        async function loadEstados(){
            const response = await api.get('/estados');
            
            setEstados(response.data);
        }

        loadEstados();

        async function loadTipos(){
            const response = await api.get('/tiposeventos');

            setTipos(response.data);
           
        }
        loadTipos();

         async function loadTemas(){
            const response = await api.get('/temaestabelecimento');

            setTemas(response.data);
        }
        loadTemas();
        

    },[])

    function handleSelect(e){
        let id 
        setEscolhaCidade('')
        setEstadoEscolhido(e.target.value)
        console.log(e.target.value)
        
        id =  e.target.value

        async function loadCidades(){
            const response = await api.get(`/cidades/${id}`);
            
            setCidades(response.data);
            
        }
        
        loadCidades();

    }
    function filtrar(){

        console.log(estadoEscolhido)
        console.log(escolhaCidade)
        console.log(escolhaTipos)
        console.log(escolhaTema)
        let dados = `${estadoEscolhido}&${escolhaCidade}&${escolhaTipos}&${escolhaTema}`

        history.push(`/filter/${dados}`)
        
    }
   

return(

    <section>
        <div className="container-filtro">
            <div className="title-filtro">
                <h1>Lorem ipsum dolor sit amet</h1>
                <h2>consectetur adipiscing elit. Pellentesque placerat dictum risus, sed</h2>
            </div>
            <div className="body-filtro">
                <div className="type-filter">
                <ul> 
                    <li className="type-select-filter"><h1>Casamento</h1></li>
                    <li><h1>Lorem ipsum dolor</h1></li>
                    <li><h1>Lorem ipsum dolor</h1></li>
                    <li><h1>Lorem ipsum dolor</h1></li>
                </ul>
                </div>
                <div className="img-filter">
                    <img src={casados} alt="imagem"/>
                </div>
                <div className="form-filter">
                    <select onChange={handleSelect}>
                    <option value="estado">Selecione o Estado</option>
                        {estados.map(estado => (
                        <option value={estado.id} key={estado.id}>
                            {estado.nome}
                        </option>
                        ))}
                    </select>
                    <select onChange={e => setEscolhaCidade(e.target.value)}>                    
                        <option>Selecione a cidade</option>
                        {cidades.map(cidade => (
                            <option value={cidade.id} key={cidade.id} >
                                {cidade.nome}
                            </option>
                        ))}
                    </select>
                    <select onChange={e => setEscolhaTipos(e.target.value)}>
                    <option>Tipo de evento</option>
                        {tipos.map(tipo => (
                            <option value={tipo.id} key={tipo.id}>{tipo.descricao}</option>
                        ))}    
                    </select>
                    <select onChange={e => setEscolhaTema(e.target.value)}>
                        <option value="tema">Tema/Estilo</option>
                        {temas.map(tema =>(
                            <option value={tema.id} key={tema.id}>
                                {tema.descricao}
                            </option>
                        ))}
                    </select>
                    <button onClick={filtrar}>Filtrar Fornecedores</button>
                </div>

            </div>
        </div>


    </section>
    
)
}