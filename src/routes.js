import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from './pages/Home';
import Anuncio from './pages/Anuncio';
import listagemFiltro from './pages/Filtro/components/listagem'

export default function Routes(){
    return (
    
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/filter" component={listagemFiltro}>
                    <Route path="/filter/:dados" component={listagemFiltro}/>
                </Route>
                 
                <Route path="/anuncio/:id" component={Anuncio}/>
                
            </Switch>
    
    );
}