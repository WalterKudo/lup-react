import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import Routes from './routes';
import Header from './components/header/Header'
import '../src/App.css'

function App() {
  return (
    <>
    <BrowserRouter>
      <Header/>
      <Routes />
    </BrowserRouter>
    </>
  );
}

export default App;
